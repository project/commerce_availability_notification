<?php

namespace Drupal\commerce_availability_notification;

use Drupal\commerce\Context;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Availability notification entity.
 *
 * @see \Drupal\commerce_availability_notification\Entity\AvailabilityNotification.
 */
class AvailabilityNotificationAccessControlHandler extends EntityAccessControlHandler {

  /**
   * Permission for administering availability notifications.
   */
  const ADMIN_PERMISSION = 'administer availability notification entities';

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, self::ADMIN_PERMISSION);

      case 'send':
        $last_sent = $entity->get('last_sent')->value;
        $sent = AccessResult::allowedIf(empty($last_sent))->addCacheableDependency($entity);

        $available = AccessResult::neutral();
        /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $product_variation */
        $product_variation = $entity->getPurchasableEntity();
        if ($product_variation) {
          $context = new Context($entity->getOwner(), $product_variation->getStores()[0]);
          /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
          $order_item = \Drupal::entityTypeManager()->getStorage('commerce_order_item')
            ->create([
              'type' => 'default',
              'purchased_entity' => $product_variation,
              'quantity' => 1,
              'unit_price' => $product_variation->getPrice(),
            ]);

          /** @var \Drupal\commerce_order\AvailabilityResult $check */
          $check = \Drupal::service('commerce_order.availability_manager')->check($order_item, $context);
          $available = AccessResult::allowedIf(!$check->isUnavailable())->addCacheableDependency($product_variation);
        }
        return AccessResult::allowedIfHasPermission($account, self::ADMIN_PERMISSION)
          ->andIf($sent)
          ->andIf($available);

      case 'delete':
        if ($account->hasPermission(self::ADMIN_PERMISSION)) {
          return AccessResult::allowed();
        }
        return AccessResult::allowedIf(!$account->isAnonymous() && $account->id() === $entity->getOwnerId());
    }
    return AccessResult::forbidden();
  }

}
