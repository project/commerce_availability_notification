<?php

namespace Drupal\commerce_availability_notification;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for Availability notification entities.
 *
 * @see \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see \Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class AvailabilityNotificationHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    if ($send_notification_form_route = $this->getSendNotificationFormRoute($entity_type)) {
      $collection->add("entity.commerce_availability_notif.send_notification_form", $send_notification_form_route);
    }

    return $collection;
  }

  /**
   * Gets the send-notification-form route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getSendNotificationFormRoute(EntityTypeInterface $entity_type) {
    $route = new Route($entity_type->getLinkTemplate('send-notification-form'));
    $route
      ->addDefaults([
        '_entity_form' => 'commerce_availability_notif.send-notification',
        '_title_callback' => '\Drupal\Core\Entity\Controller\EntityController::title',
      ])
      ->setRequirement('_entity_access', 'commerce_availability_notif.view')
      ->setRequirement('commerce_availability_notif', '\d+')
      ->setOption('parameters', [
        'commerce_availability_notif' => [
          'type' => 'entity:commerce_availability_notif',
        ],
      ])
      ->setOption('_admin_route', TRUE);

    return $route;
  }

}
