<?php

namespace Drupal\commerce_availability_notification;

use Drupal\commerce\CommerceContentEntityStorage;
use Drupal\commerce\PurchasableEntityInterface;

/**
 * Storage class.
 */
class AvailabilityNotificationStorage extends CommerceContentEntityStorage implements AvailabilityNotificationStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadMultipleByEntity(PurchasableEntityInterface $entity, $include_sent = FALSE) {
    // Build a query to fetch the entity IDs.
    $entity_query = $this->getQuery();
    $entity_query->accessCheck(FALSE);
    $entity_query->condition('entity_type', $entity->getEntityTypeId());
    $entity_query->condition('entity_id', $entity->id());
    if (!$include_sent) {
      $entity_query->condition('last_sent', NULL, 'IS NULL');
    }
    $result = $entity_query->execute();
    return $result ? $this->loadMultiple($result) : [];
  }

}
