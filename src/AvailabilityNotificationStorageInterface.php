<?php

namespace Drupal\commerce_availability_notification;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines the interface for commerce availability notification storage.
 */
interface AvailabilityNotificationStorageInterface extends ContentEntityStorageInterface {

  /**
   * Loads all notifications for the purchasable entity.
   *
   * @param \Drupal\commerce\PurchasableEntityInterface $entity
   *   The purchasable entity.
   * @param bool $include_sent
   *   Include sent notifications, FALSE by default.
   *
   * @return \Drupal\commerce_availability_notification\Entity\AvailabilityNotificationInterface[]
   *   The notifications.
   */
  public function loadMultipleByEntity(PurchasableEntityInterface $entity, $include_sent = FALSE);

}
