<?php

namespace Drupal\commerce_availability_notification\Commands;

use Drupal\Core\Lock\LockBackendInterface;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 */
class CommerceAvailabilityNotificationCommands extends DrushCommands {

  /**
   * The lock.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * The command file constructor.
   *
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock backend.
   */
  public function __construct(LockBackendInterface $lock) {
    parent::__construct();
    $this->lock = $lock;
  }

  /**
   * Send notifications.
   *
   * @command commerce_availability_notification:send
   */
  public function send() {
    if (!$this->lock->acquire('commerce_availability_notification', 3600)) {
      $this->logger()->error('Another notification process is already running.');
      return;
    }
    _commerce_availability_notification_send_cron();
    $this->logger()->success(dt('The notification list was processed.'));
  }

}
