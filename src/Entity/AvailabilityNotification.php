<?php

namespace Drupal\commerce_availability_notification\Entity;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\UserInterface;

/**
 * Defines the Availability notification entity.
 *
 * @ingroup commerce_availability_notification
 *
 * @ContentEntityType(
 *   id = "commerce_availability_notif",
 *   label = @Translation("Availability notification"),
 *   handlers = {
 *     "storage" = "Drupal\commerce_availability_notification\AvailabilityNotificationStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\commerce_availability_notification\Entity\AvailabilityNotificationViewsData",
 *     "list_builder" = "Drupal\commerce_availability_notification\AvailabilityNotificationListBuilder",
 *     "access" = "Drupal\commerce_availability_notification\AvailabilityNotificationAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\commerce_availability_notification\AvailabilityNotificationHtmlRouteProvider",
 *     },
 *     "form" = {
 *       "delete" = "Drupal\commerce_availability_notification\Form\AvailabilityNotificationDeleteForm",
 *       "send-notification" = "Drupal\commerce_availability_notification\Form\AvailabilityNotificationSendForm",
 *     },
 *   },
 *   base_table = "commerce_availability_notif",
 *   admin_permission = "administer availability notification entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "email",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "collection" = "/admin/commerce/commerce_availability_notification",
 *     "send-notification-form" = "/admin/commerce/commerce_availability_notification/{commerce_availability_notif}/send",
 *     "delete-form" = "/admin/commerce/commerce_availability_notification/{commerce_availability_notif}/delete",
 *     "delete-multiple-form" = "/admin/commerce/commerce_availability_notification/delete",
 *   },
 * )
 */
class AvailabilityNotification extends ContentEntityBase implements AvailabilityNotificationInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['langcode']->setDisplayConfigurable('form', TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Created by'))
      ->setDescription(t('The user who subscribed to this notification'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['email'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Email'))
      ->setDescription(t('A email where to send the notification'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity type'))
      ->setRequired(TRUE)
      ->setDescription(t('The entity type to which this comment is attached.'))
      ->setSetting('is_ascii', TRUE)
      ->setSetting('max_length', EntityTypeInterface::ID_MAX_LENGTH);

    $fields['entity_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Entity ID'))
      ->setDescription(t('The ID of the entity of which this comment is a reply.'))
      ->setRequired(TRUE);

    // @todo Only commerce_product_Variation is currently supported.
    /** @var \Drupal\commerce\PurchasableEntityTypeRepositoryInterface $purchasable_entity_type_repository */
    $purchasable_entity_type_repository = \Drupal::service('commerce.purchasable_entity_type_repository');
    $default_purchasable_entity_type = $purchasable_entity_type_repository->getDefaultPurchasableEntityType();
    if ($default_purchasable_entity_type) {
      $fields['entity_id']->setSetting('target_type', $default_purchasable_entity_type->id());
    }

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created on'))
      ->setDescription(t('The time that the notification request was created.'))
      ->setDisplayOptions('view', [
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['last_sent'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Sent on'))
      ->setDescription(t('The time that the notification was last sent to the user.'))
      ->setSetting('datetime_type', 'datetime')
      ->setDefaultValue(NULL)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 5,
      ]);
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getPurchasableEntity() {
    return $this->get('entity_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setPurchasableEntity(PurchasableEntityInterface $entity) {
    return $this->set('entity_type', $entity->getEntityTypeId())->set('entity_id', $entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getPurchasableEntityId() {
    return $this->get('entity_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getPurchasableEntityTypeId() {
    return $this->get('entity_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmail() {
    return $this->get('email')->getString();
  }

  /**
   * {@inheritdoc}
   */
  public function setEmail($email) {
    $this->set('email', $email);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

}
