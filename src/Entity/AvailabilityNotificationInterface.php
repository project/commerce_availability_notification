<?php

namespace Drupal\commerce_availability_notification\Entity;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Availability notification entities.
 *
 * @ingroup commerce_availability_notification
 */
interface AvailabilityNotificationInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * Gets the Purchasable Entity from Notification entity.
   *
   * @return \Drupal\commerce\PurchasableEntityInterface|null
   *   The purchasable entity.
   */
  public function getPurchasableEntity();

  /**
   * Gets the Purchasable Entity ID.
   *
   * @return int|null
   *   The purchasable entity.
   */
  public function getPurchasableEntityId();

  /**
   * Gets the Purchasable Entity Type ID.
   *
   * @return int|null
   *   The purchasable entity.
   */
  public function getPurchasableEntityTypeId();

  /**
   * Sets the Purchasable Entity from Notification entity.
   *
   * @param \Drupal\commerce\PurchasableEntityInterface $entity
   *   The purchasable entity.
   *
   * @return \Drupal\commerce_availability_notification\Entity\AvailabilityNotificationInterface
   *   The Notification entity.
   */
  public function setPurchasableEntity(PurchasableEntityInterface $entity);

  /**
   * Gets the email address that the email will be sent to.
   *
   * @return string
   *   The email address.
   */
  public function getEmail();

  /**
   * Set an email address that the email will be sent to.
   *
   * @param string $email
   *   The email address.
   *
   * @return \Drupal\commerce_availability_notification\Entity\AvailabilityNotificationInterface
   *   The Notification entity.
   */
  public function setEmail($email);

  /**
   * Get the creation timestamp.
   *
   * @return int
   *   The timestamp.
   */
  public function getCreatedTime();

  /**
   * Set timestamp.
   *
   * @param int $timestamp
   *   The timestamp.
   *
   * @return \Drupal\commerce_availability_notification\Entity\AvailabilityNotificationInterface
   *   The Notification entity.
   */
  public function setCreatedTime($timestamp);

}
