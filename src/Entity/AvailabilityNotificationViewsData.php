<?php

namespace Drupal\commerce_availability_notification\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for availability notification entities.
 */
class AvailabilityNotificationViewsData extends EntityViewsData {

}
