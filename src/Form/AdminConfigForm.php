<?php

namespace Drupal\commerce_availability_notification\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Availability Notification configurable form.
 */
class AdminConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_availability_notification_admin_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'commerce_availability_notification.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['cron_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Cron Enabled?'),
      '#description' => $this->t('If this option is checked, emails will be send automatically with cron'),
      '#default_value' => $this->configFactory->getEditable('commerce_availability_notification.settings')->get('cron_enabled') ?? TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $form_state->cleanValues();
    $config = $form_state->getValues();

    $this->configFactory->getEditable('commerce_availability_notification.settings')
      ->set('cron_enabled', $config['cron_enabled'])
      ->save();
  }

}
