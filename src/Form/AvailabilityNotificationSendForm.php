<?php

namespace Drupal\commerce_availability_notification\Form;

use Drupal\commerce_availability_notification\NotificationSender;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides send form for Availability Notification entities.
 */
class AvailabilityNotificationSendForm extends ContentEntityConfirmFormBase {

  /**
   * The notification mail service.
   *
   * @var \Drupal\commerce_availability_notification\NotificationSender
   */
  protected $notificationSender;

  /**
   * Constructs a new AvailabilityNotificationSendForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\commerce_availability_notification\NotificationSender $notification_sender
   *   The notification mail service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, NotificationSender $notification_sender) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);

    $this->notificationSender = $notification_sender;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('commerce_availability_notification.sender')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to send the availability notification for %label?', [
      '%label' => $this->entity->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Send notification');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('<current>');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_availability_notification\Entity\AvailabilityNotificationInterface $notification */
    $notification = $this->entity;
    $result = $this->notificationSender->send($notification);
    // Drupal's MailManager sets an error message itself, if the sending failed.
    if ($result) {
      $this->messenger()->addMessage($this->t('Availability notification was sent.'));
    }
  }

}
