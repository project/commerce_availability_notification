<?php

namespace Drupal\commerce_availability_notification;

use Drupal\commerce\MailHandlerInterface;
use Drupal\commerce_availability_notification\Entity\AvailabilityNotificationInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\UserInterface;

/**
 * Takes care of actually sending notificatons.
 */
class NotificationSender {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The mail handler.
   *
   * @var \Drupal\commerce\MailHandlerInterface
   */
  protected $mailHandler;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce\MailHandlerInterface $mail_handler
   *   The mail handler.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MailHandlerInterface $mail_handler) {
    $this->entityTypeManager = $entity_type_manager;
    $this->mailHandler = $mail_handler;
  }

  /**
   * Takes care of sending the notification.
   *
   * @param \Drupal\commerce_availability_notification\Entity\AvailabilityNotificationInterface $notification
   *   The notification to be send.
   * @param bool $save
   *   Should the notification status be updated after the notification is sent?
   *
   * @return bool
   *   The send status.
   */
  public function send(AvailabilityNotificationInterface $notification, $save = TRUE) {
    if (!empty($notification->get('last_sent')->getValue())) {
      return TRUE;
    }

    $user = $notification->getOwner();
    $purchasable_entity = $notification->getPurchasableEntity();

    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
    $order_item = $this->entityTypeManager->getStorage('commerce_order_item')
      ->create([
        // @todo We need a better way of creating order item.
        'type' => 'default',
        'purchased_entity' => $purchasable_entity->id(),
        'quantity' => 1,
        'unit_price' => $purchasable_entity->getPrice(),
        'title' => $purchasable_entity->getOrderItemTitle(),
      ]);

    $subject = $this->t('@title is now available for purchase', [
      '@title' => $order_item->label(),
    ]);
    $body = [
      '#theme' => 'commerce_availability_notification',
      '#order_item' => $order_item,
      '#notification' => $notification,
    ];

    // @todo Use store email from notification entity.
    /** @var \Drupal\commerce_store\Entity\StoreInterface $store */
    $store = $purchasable_entity->getStores()[0];

    $params = [
      'id' => 'commerce_availability_notification',
      'from' => $store->getEmail(),
    ];
    if ($user instanceof UserInterface) {
      $params['langcode'] = $notification->get('langcode')->value ?: $user->getPreferredLangcode();
    }

    // Send a message.
    if ($this->mailHandler->sendMail($notification->getEmail(), $subject, $body, $params)) {
      $time = new DrupalDateTime();
      $notification->set('last_sent', $time->getTimestamp());
      if ($save) {
        $notification->save();
      }
      return TRUE;
    }
    return FALSE;
  }

}
