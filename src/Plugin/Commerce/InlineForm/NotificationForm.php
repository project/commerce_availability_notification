<?php

namespace Drupal\commerce_availability_notification\Plugin\Commerce\InlineForm;

use Drupal\commerce\Plugin\Commerce\InlineForm\EntityInlineFormBase;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an inline form for registering availability notification.
 *
 * @CommerceInlineForm(
 *   id = "commerce_availability_notif",
 *   label = @Translation("Register notification form"),
 * )
 */
class NotificationForm extends EntityInlineFormBase {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new Notification object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      // The account_id is passed via configuration to avoid serialization.
      'account_id' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildInlineForm(array $inline_form, FormStateInterface $form_state) {
    $inline_form = parent::buildInlineForm($inline_form, $form_state);

    if (!$this->getEntity() instanceof PurchasableEntityInterface) {
      throw new \LogicException('Invalid purchasable entity given to the commerce_availability_notification inline form.');
    }
    $account = $this->entityTypeManager->getStorage('user')->load($this->configuration['account_id']);

    $inline_form['notification'] = [
      '#type' => 'container',
      'account' => [
        '#type' => 'value',
        '#value' => $account,
      ],
      'entity_id' => [
        '#type' => 'value',
        '#value' => $this->getEntity()->id(),
      ],
      'entity_type' => [
        '#type' => 'value',
        '#value' => $this->getEntity()->getEntityTypeId(),
      ],
      'entity' => [
        '#type' => 'value',
        '#value' => $this->getEntity(),
      ],
      'notify_email' => [
        '#type' => 'email',
        '#title' => $this->t('Email'),
        '#required' => TRUE,
        '#default_value' => $account instanceof UserInterface ? $account->getEmail() : '',
      ],
      'notify_submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Notify me'),
        '#limit_validation_errors' => [
          $inline_form['#parents'],
        ],
        '#submit' => [
          [get_called_class(), 'saveNotification'],
        ],
        '#ajax' => [
          'callback' => [get_called_class(), 'ajaxRefreshForm'],
          'element' => $inline_form['#parents'],
        ],
      ],
    ];

    return $inline_form;
  }

  /**
   * Empty submit callback to prevent add to cart submit handler to be called.
   */
  public static function saveNotification(array $form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $parents = array_slice($triggering_element['#parents'], 0, -1);
    $inline_form = NestedArray::getValue($form, $parents);

    if (count($form_state->getErrors()) === 0) {

      $notify_email = $form_state->getValue(array_merge($parents, ['notify_email']));
      $account = $form_state->getValue(array_merge($parents, ['account']));
      $entity = $form_state->getValue(array_merge($parents, ['entity']));

      // Check if already subscribed.
      $storage = \Drupal::entityTypeManager()->getStorage('commerce_availability_notif');
      $notification_exists = $storage->getQuery()
        ->condition('email', $notify_email)
        ->condition('entity_id', $entity->id())
        ->condition('entity_type', $entity->getEntityTypeId())
        ->execute();
      // Create notification only if one doesn't exist, otherwise reset
      // last_sent flag to NULL.
      if ($notification_exists && ($notification_id = reset($notification_exists))) {
        /** @var \Drupal\commerce_availability_notification\Entity\AvailabilityNotificationInterface $notification */
        $notification = $storage->load($notification_id);
        $notification->set('last_sent', NULL);
        $notification->save();
      }
      else {
        $storage->create([
          'user_id' => $account->id(),
          'entity_id' => $entity->id(),
          'entity_type' => $entity->getEntityTypeId(),
          'created' => \Drupal::time()->getRequestTime(),
          'email' => $notify_email,
          'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
        ])->save();
      }
      \Drupal::messenger()->addStatus(t('Notification registered.'));
    }
    $form_state->setRebuild();

  }

}
