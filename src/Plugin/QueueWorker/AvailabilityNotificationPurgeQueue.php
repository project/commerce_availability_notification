<?php

namespace Drupal\commerce_availability_notification\Plugin\QueueWorker;

use Drupal\commerce_availability_notification\Entity\AvailabilityNotificationInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sends out notifications when items are in stock.
 *
 * @QueueWorker(
 *   id = "commerce_availability_notification_cleanup",
 *   title = @Translation("Commerce Availability Notification Purge Queue"),
 *   cron = {"time" = 30}
 * )
 */
class AvailabilityNotificationPurgeQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * AvailabilityNotificationQueue constructor.
   *
   * @param array $configuration
   *   Configuration.
   * @param mixed $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin Definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($notification) {
    if (empty($notification['notification_id'])) {
      return;
    }
    $notification = $this->entityTypeManager->getStorage('commerce_availability_notif')->load($notification['notification_id']);
    if (!$notification instanceof AvailabilityNotificationInterface) {
      return;
    }
    $notification->delete();
  }

}
