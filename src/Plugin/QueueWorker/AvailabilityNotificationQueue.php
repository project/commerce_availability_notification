<?php

namespace Drupal\commerce_availability_notification\Plugin\QueueWorker;

use Drupal\commerce_availability_notification\Entity\AvailabilityNotificationInterface;
use Drupal\commerce_availability_notification\NotificationSender;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sends out notifications when items are in stock.
 *
 * @QueueWorker(
 *   id = "commerce_availability_notification",
 *   title = @Translation("Commerce Availability Notification Queue"),
 *   cron = {"time" = 30}
 * )
 */
class AvailabilityNotificationQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The notification sender.
   *
   * @var \Drupal\commerce_availability_notification\NotificationSender
   */
  protected $notificationSender;

  /**
   * AvailabilityNotificationQueue constructor.
   *
   * @param array $configuration
   *   Configuration.
   * @param mixed $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin Definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_availability_notification\NotificationSender $notification_sender
   *   The notification sender.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, NotificationSender $notification_sender) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->notificationSender = $notification_sender;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('commerce_availability_notification.sender'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($notification) {
    if (empty($notification['notification_id'])) {
      return;
    }
    $notification = $this->entityTypeManager->getStorage('commerce_availability_notif')->load($notification['notification_id']);
    if (!$notification instanceof AvailabilityNotificationInterface) {
      return;
    }

    $this->notificationSender->send($notification, TRUE);
  }

}
