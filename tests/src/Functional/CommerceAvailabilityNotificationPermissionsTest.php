<?php

namespace Drupal\Tests\commerce_availability_notification\tests\Functional;

use Drupal\commerce_order_test\TestAvailabilityChecker;
use Drupal\commerce_store\Entity\Store;
use Drupal\Tests\commerce_cart\Functional\CartBrowserTestBase;

/**
 * Tests Commerce Availability Notification permissions.
 *
 * @group commerce_availability_notification
 */
class CommerceAvailabilityNotificationPermissionsTest extends CartBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce_availability_notification',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();
    $this->user = $this->drupalCreateUser(['access content']);
    $this->user_with_permission = $this->drupalCreateUser([
      'access content',
      'subscribe to availability notifications',
    ]);
    $this->drupalLogin($this->user_with_permission);

    $this->availableManager = $this->container->get('commerce_order.availability_manager');
    $this->availableManager->addChecker(new TestAvailabilityChecker());

    $this->store = Store::create([
      'type' => 'default',
      'label' => 'My store',
    ]);
    $this->store->save();

    $this->variation1 = $this->createEntity('commerce_product_variation', [
      'type' => 'default',
      'sku' => $this->randomString(),
      'price' => [
        'number' => '5.00',
        'currency_code' => 'NOK',
      ],
    ]);

    $this->variation2 = $this->createEntity('commerce_product_variation', [
      'type' => 'default',
      'sku' => 'TEST_ASD',
      'price' => [
        'number' => '5.00',
        'currency_code' => 'NOK',
      ],
    ]);
    $this->createEntity('commerce_product', [
      'type' => 'default',
      'title' => $this->randomMachineName(),
      'stores' => [$this->store],
      'variations' => [$this->variation1, $this->variation2],
    ]);
  }

  /**
   * Tests showing the availability form if product available or not.
   */
  public function testAvailableForm() {
    $this->drupalGet($this->variation1->toUrl());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'form.commerce-order-item-add-to-cart-form');
    $this->assertSession()->elementNotExists('css', '#edit-commerce-availability-notif-notification');

    // This variation has SKU that contains "TEST_" string it's will equal
    // to not available product.
    $this->drupalGet($this->variation2->toUrl());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'form.commerce-order-item-add-to-cart-form');
    $this->assertSession()->elementExists('css', '#edit-commerce-availability-notif-notification');
  }

  /**
   * Tests showing the availability form if user have permission.
   */
  public function testAvailableFormPermissions() {
    $this->drupalLogin($this->user);

    $this->drupalGet($this->variation2->toUrl());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'form.commerce-order-item-add-to-cart-form');
    $this->assertSession()->elementNotExists('css', '#edit-commerce--availability-notif-notification');

    $this->drupalLogin($this->user_with_permission);
    // This variation has SKU that contains "TEST_" string it's will equal
    // to not available product.
    $this->drupalGet($this->variation2->toUrl());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'form.commerce-order-item-add-to-cart-form');
    $this->assertSession()->elementExists('css', '#edit-commerce-availability-notif-notification');
  }

}
